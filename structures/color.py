from dataclasses import dataclass, field
import colorsys

from .hsb import Hue, Saturation


@dataclass
class Color:
    hue: Hue
    saturation: Saturation = field(default_factory=lambda: Saturation(100))

    def to_rgb(self):
        return "0 255 255"
        # return colorsys.hsv_to_rgb(
        #     h=self.hue.value / 100,
        #     s=self.saturation.value / 100,
        #     v=1,
        # )
