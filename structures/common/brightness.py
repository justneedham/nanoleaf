from ..hsb import Brightness


DIM = Brightness(15)
LOW = Brightness(30)
MODERATE = Brightness(50)
HIGH = Brightness(75)
BRIGHT = Brightness(100)
