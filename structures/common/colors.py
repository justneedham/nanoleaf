from ..hsb import Hue
from ..color import Color

from enum import Enum

RED = Color(hue=Hue(360))
ORANGE = Color(hue=Hue(30))
YELLOW = Color(hue=Hue(60))
GREEN = Color(hue=Hue(120))
BLUE = Color(hue=Hue(240))
VIOLET = Color(hue=Hue(300))


class Colors(Enum):
    RED = Color(hue=Hue(360))
    ORANGE = Color(hue=Hue(30))
    YELLOW = Color(hue=Hue(60))
    GREEN = Color(hue=Hue(120))
    BLUE = Color(hue=Hue(240))
    VIOLET = Color(hue=Hue(300))
