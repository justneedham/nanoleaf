from dataclasses import dataclass


@dataclass
class Hue:
    value: int

    def __post_init__(self):
        if self.value < 0 or self.value > 360:
            raise ValueError("hue must be a value between 0 and 360")


@dataclass
class Saturation:
    value: int

    def __post_init__(self):
        if self.value < 0 or self.value > 100:
            raise ValueError("saturation must be a value between 0 and 100")


@dataclass
class Brightness:
    value: int

    def __post_init__(self):
        if self.value < 0 or self.value > 100:
            raise ValueError("brightness must be a value between 0 and 100")
