from apis import NanoLeafAPI, NanoLeafConfig, Panel

from structures import Brightness, Color
from structures.common import colors
from .light import Light


class NanoLeafLightSet(Light):
    def __init__(
        self, name: str, ip: str, port: int = 16021, api_key: str | None = None
    ):
        self.name = name
        self.config = NanoLeafConfig(
            ip=ip,
            port=port,
            api_key=api_key,
        )

    def turn_on(self) -> None:
        NanoLeafAPI.state.turn_on(config=self.config)

    def turn_off(self) -> None:
        NanoLeafAPI.state.turn_off(config=self.config)

    def set_brightness(self, brightness: Brightness) -> None:
        NanoLeafAPI.brightness.set(config=self.config, brightness=brightness)

    def set_color(self, color: Color) -> None:
        NanoLeafAPI.hue.set(config=self.config, hue=color.hue)
        NanoLeafAPI.saturation.set(config=self.config, saturation=color.saturation)

    def get_layout(self):
        NanoLeafAPI.layout.get(config=self.config)
        NanoLeafAPI.effects.set(config=self.config, panels=[
            Panel(panel_id=22261, color=colors.GREEN),
            Panel(panel_id=20606, color=colors.GREEN),
            Panel(panel_id=41278, color=colors.GREEN),
            Panel(panel_id=50016, color=colors.GREEN),
            Panel(panel_id=28349, color=colors.GREEN),
            Panel(panel_id=12832, color=colors.GREEN),
            Panel(panel_id=13453, color=colors.GREEN),
            Panel(panel_id=54492, color=colors.GREEN),
            Panel(panel_id=18594, color=colors.GREEN),
            Panel(panel_id=47586, color=colors.GREEN),
            Panel(panel_id=6929, color=colors.GREEN),
            Panel(panel_id=59985, color=colors.GREEN),
        ])

