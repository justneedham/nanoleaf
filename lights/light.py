from abc import abstractmethod
from typing import Protocol

from structures import Brightness, Color


class Light(Protocol):
    @abstractmethod
    def turn_on(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def turn_off(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def set_color(self, color: Color) -> None:
        raise NotImplementedError

    @abstractmethod
    def set_brightness(self, brightness: Brightness) -> None:
        raise NotImplementedError
