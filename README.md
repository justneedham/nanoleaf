# Nanoleaf

## Getting Started

1. Create Virtual Environment
   ```bash
    python -m venv venv
    ```
2. Install dependencies
    ```bash
    pip install -r requirements.txt
    ```
3. Run program
    ```bash
    python main.py
    ```