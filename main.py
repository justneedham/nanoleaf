import time

from lights import NanoLeafLightSet
from structures.common import brightness, colors
from structures import Color, Hue, Saturation
from config import (
    LINES_IP,
    LINES_API_KEY,
    PANELS_IP,
    PANELS_API_KEY,
)


def main():
    plant_wall = NanoLeafLightSet(
        name="plant_wall",
        ip=LINES_IP,
        api_key=LINES_API_KEY,
    )

    plant_wall.turn_on()
    plant_wall.set_brightness(brightness=brightness.BRIGHT)

    my_color = Color(
        hue=Hue(330),
        saturation=Saturation(80),
    )
    plant_wall.set_color(color=colors.ORANGE)
    # plant_wall.get_layout()


if __name__ == "__main__":
    main()
