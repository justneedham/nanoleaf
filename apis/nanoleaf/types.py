from enum import Enum

from structures import Color


class NanoLeafConfig:
    def __init__(self, ip: str, api_key: str, port: int = 16021):
        self.ip = ip
        self.port = port
        self.api_key = api_key

    @property
    def base_url(self) -> str:
        return f"http://{self.ip}:{self.port}/api/v1/{self.api_key}"


class Panel:

    def __init__(self, panel_id: int, color: Color):
        self.id = panel_id
        self.color = color

    def set_color(self, color: Color) -> None:
        self.color = color

    @property
    def anim_data(self) -> str:
        return f"{self.id} 1 {self.color.to_rgb()} 0 0"


class AnimationType(Enum):
    RANDOM = "random"
    FLOW = "flow"
    WHEEL = "wheel"
    FADE = "fade"
    HIGHLIGHT = "highlight"
    CUSTOM = "custom"
    STATIC = "static"


class CommandType(Enum):
    ADD = "add"
    REQUEST = "request"
    DELETE = "delete"
    DISPLAY = "display"
    RENAME = "rename"
    REQUEST_ALL = "request_all"
