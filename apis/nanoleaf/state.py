import requests
import structlog
import json

from .types import NanoLeafConfig
from utils import handle_response

logger = structlog.getLogger()


class StateAPI:
    @staticmethod
    def turn_on(config: NanoLeafConfig) -> None:
        data = {
            "on": {
                "value": True,
            }
        }
        handle_response(
            requests.put(
                url=f"{config.base_url}/state",
                data=json.dumps(data),
            ),
            success_message="turned light on",
        )

    @staticmethod
    def turn_off(config: NanoLeafConfig) -> None:
        data = {
            "on": {
                "value": False,
            }
        }
        handle_response(
            requests.put(url=f"{config.base_url}/state", data=json.dumps(data)),
            success_message="turned light off",
        )
