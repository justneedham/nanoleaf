import requests
import structlog
import json

from .types import NanoLeafConfig, Panel, CommandType, AnimationType
from utils import handle_response


logger = structlog.getLogger()


class EffectsAPI:

    @staticmethod
    def set(config: NanoLeafConfig, panels: list[Panel]) -> None:
        data = {
            "write": {
                "command": CommandType.DISPLAY.value,
                "animType": AnimationType.STATIC.value,
                "animData": EffectsAPI._get_anim_data(panels=panels),
                "loop": False,
                "palette": [],
                "colorType": "HSB"
            }
        }
        print(data)
        handle_response(
            requests.put(
                url=f"{config.base_url}/effects",
                data=json.dumps(data),
            ),
            success_message="set effect",
        )

    @staticmethod
    def _get_anim_data(panels: list[Panel]) -> str:
        number_of_panels = len(panels)
        panels_anim_data = [panel.anim_data for panel in panels]

        data = f"{number_of_panels} {' '.join(panels_anim_data)}"
        print(data)
        return data

