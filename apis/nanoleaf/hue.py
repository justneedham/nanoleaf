import requests
import structlog
import json

from .types import NanoLeafConfig
from utils import handle_response
from structures import Hue

logger = structlog.getLogger()


class HueAPI:
    @staticmethod
    def set(config: NanoLeafConfig, hue: Hue) -> None:
        data = {
            "hue": {
                "value": hue.value,
            }
        }
        handle_response(
            requests.put(
                url=f"{config.base_url}/state",
                data=json.dumps(data),
            ),
            success_message='set hue',
            hue=hue.value,
        )
