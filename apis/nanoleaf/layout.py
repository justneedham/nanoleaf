import requests
import structlog

from .types import NanoLeafConfig
from utils import handle_response

logger = structlog.getLogger()


class LayoutAPI:
    @staticmethod
    def get(config: NanoLeafConfig) -> None:
        response = handle_response(
            requests.get(
                url=f"{config.base_url}/panelLayout/layout",
            ),
            success_message="set brightness",
        )

        data = response.json()

        print(len(data['positionData']))

        for panel in data['positionData']:
            print(panel)
