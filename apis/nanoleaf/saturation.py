import requests
import structlog
import json

from .types import NanoLeafConfig
from utils import handle_response
from structures import Saturation

logger = structlog.getLogger()


class SaturationAPI:
    @staticmethod
    def set(config: NanoLeafConfig, saturation: Saturation) -> None:
        data = {
            "sat": {
                "value": saturation.value,
            }
        }
        handle_response(
            requests.put(
                url=f"{config.base_url}/state/sat",
                data=json.dumps(data),
            ),
            success_message='set saturation',
            saturation=saturation.value,
        )
