from .state import StateAPI
from .hue import HueAPI
from .brightness import BrightnessAPI
from .saturation import SaturationAPI
from .layout import LayoutAPI
from .effects import EffectsAPI


class NanoLeafAPI:
    state = StateAPI
    hue = HueAPI
    brightness = BrightnessAPI
    saturation = SaturationAPI
    layout = LayoutAPI
    effects = EffectsAPI
