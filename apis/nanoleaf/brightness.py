import requests
import structlog
import json

from .types import NanoLeafConfig
from utils import handle_response
from structures import Brightness

logger = structlog.getLogger()


class BrightnessAPI:
    @staticmethod
    def set(config: NanoLeafConfig, brightness: Brightness) -> None:
        data = {
            "brightness": {
                "value": brightness.value,
            }
        }
        handle_response(
            requests.put(
                url=f"{config.base_url}/state",
                data=json.dumps(data),
            ),
            success_message="set brightness",
            brightness=brightness.value,
        )
