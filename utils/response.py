from requests.models import Response
import structlog

logger = structlog.getLogger()


def handle_response(
    response: Response,
    success_codes: list[int] | None = None,
    success_message: str = "request_success",
    error_message: str = "request_error",
    **kwargs,
) -> Response:
    if success_codes is None:
        success_codes = [200, 201, 204]

    if response.status_code in success_codes:
        logger.info(success_message, status_code=response.status_code, **kwargs)

    else:
        logger.error(error_message, status_code=response.status_code, **kwargs)

    return response
